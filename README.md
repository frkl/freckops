[![PyPI status](https://img.shields.io/pypi/status/freckops.svg)](https://pypi.python.org/pypi/freckops/)
[![PyPI version](https://img.shields.io/pypi/v/freckops.svg)](https://pypi.python.org/pypi/freckops/)
[![PyPI pyversions](https://img.shields.io/pypi/pyversions/freckops.svg)](https://pypi.python.org/pypi/freckops/)
[![Pipeline status](https://gitlab.com/frkl/freckops/badges/develop/pipeline.svg)](https://gitlab.com/frkl/freckops/pipelines)
[![Code style](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/ambv/black)

# freckops

*DevOps little helper*

## Installation

Download url

 - dev: https://s3-eu-west-1.amazonaws.com/dev.dl.frkl.io/linux-gnu/freckops
 - stable: TBD

## Description

Documentation still to be done.

# Development

Assuming you use [pyenv](https://github.com/pyenv/pyenv) and [pyenv-virtualenv](https://github.com/pyenv/pyenv-virtualenv) for development, here's how to setup a 'freckops' development environment manually:

    pyenv install 3.7.3
    pyenv virtualenv 3.7.3 freckops
    git clone https://gitlab.com/frkl/freckops
    cd <freckops_dir>
    pyenv local freckops
    pip install -e .[all-dev]
    pre-commit install


## Copyright & license

Please check the [LICENSE](/LICENSE) file in this repository (it's a short license!), also check out the [*freckles* license page](https://freckles.io/license) for more details.

[Parity Public License 6.0.0](https://licensezero.com/licenses/parity)

[Copyright (c) 2019 frkl OÜ](https://frkl.io)
