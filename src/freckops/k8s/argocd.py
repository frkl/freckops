# -*- coding: utf-8 -*-
import asyncio
import json
import os
import secrets
from typing import TYPE_CHECKING, Any, Optional

import asyncclick as click
from aiohttp import ClientResponse
from aiohttp.client import ClientSession, _RequestContextManager
from freckops.defaults import FRECKOPS_KUBERNETES_MANIFESTS
from freckops.k8s.kube import Kube, KubeCtl
from freckops.utils import FreckOpsTaskDesc
from frtls.exceptions import FrklException
from frtls.networking import find_free_port
from frtls.tasks import TaskDesc
from ruamel.yaml import YAML


if TYPE_CHECKING:
    from freckops.freckops import FreckOps

yaml = YAML()


class ArgoCDSession(KubeCtl):
    def __init__(
        self,
        kube: Kube,
        argo_password: str,
        argo_user: str = "admin",
        namespace: str = "defualt",
        verify_ssl: bool = False,
    ):

        self._port: int = find_free_port(8080)
        command = "port-forward"
        args = ["svc/argocd-server", "-n", namespace, f"{self._port}:443"]

        super().__init__(kube, command, *args)

        self._argo_user: str = argo_user
        self._argo_password: str = argo_password
        self._session: Optional[ClientSession] = None
        self._verify_ssl: bool = verify_ssl

    @property
    def port(self) -> int:
        return self._port

    @property
    def session(self) -> Optional[ClientSession]:
        return self._session

    @property
    def forwarded_port(self):

        return self._port

    def api_url(self, path: str = None):

        base = f"https://localhost:{self.port}/api/v1/"
        if path:
            if path.startswith("/"):
                path = path[1:]
            return f"{base}{path}"
        else:
            return base

    def get(
        self, path: str, allow_redirects: bool = True, **kwargs
    ) -> _RequestContextManager:

        return self._session.get(  # type: ignore
            self.api_url(path),
            allow_redirects=allow_redirects,
            ssl=self._verify_ssl,
            **kwargs,
        )

    def post(self, path: str, data: Any = None, **kwargs) -> _RequestContextManager:

        return self._session.post(  # type: ignore
            self.api_url(path), data=data, ssl=self._verify_ssl, **kwargs
        )

    def put(self, path: str, data: Any = None, **kwargs) -> _RequestContextManager:

        return self._session.put(  # type: ignore
            self.api_url(path), data=data, ssl=self._verify_ssl, **kwargs
        )

    def delete(self, path: str, **kwargs) -> _RequestContextManager:

        return self._session.delete(  # type: ignore
            self.api_url(path), ssl=self._verify_ssl, **kwargs
        )

    async def __aenter__(self):

        await super(ArgoCDSession, self).__aenter__()

        # TODO: check port to be ready
        await asyncio.sleep(1)
        self._session = ClientSession()
        data = {"username": self._argo_user, "password": self._argo_password}
        url = self.api_url(path="/session")

        response = await self._session.post(
            url, data=json.dumps(data).encode(), ssl=self._verify_ssl
        )

        if response.status != 200:
            await self._session.close()
            resp = await response.text()
            raise Exception(f"Can't login to argo: {resp}")

        return self

    async def __aexit__(self, *args):

        await self._session.close()
        await super(ArgoCDSession, self).__aexit__(*args)


class ArgoCD(object):
    def __init__(
        self,
        freckops: "FreckOps",
        kube: Kube,
        argo_user: str = "admin",
        argo_password: str = None,
        argo_namespace: str = "default",
    ):

        self._freckops: FreckOps = freckops
        self._kube: Kube = kube
        self._argo_user: str = argo_user
        self._argo_password: Optional[str] = argo_password
        self._argo_namespace = argo_namespace

        self._manifest_folder: str = os.path.join(
            FRECKOPS_KUBERNETES_MANIFESTS, "argo-cd"
        )

    async def ensure_manifests(self, parent_task_desc: TaskDesc = None):

        target = {"target": self._manifest_folder, "merge_strategy": "overwrite"}

        await self._freckops.install_pkg(
            target=target, pkg_name="argo-cd", parent_task_desc=parent_task_desc
        )

    async def install(
        self,
        admin_password: Optional[str] = None,
        wait: bool = True,
        parent_task_desc: TaskDesc = None,
    ):

        td_install = FreckOpsTaskDesc(
            name="installing argocd",
            msg="installing argocd manifests",
            parent=parent_task_desc,
        )
        td_install.task_started()

        await self.ensure_manifests(parent_task_desc=td_install)

        install_yaml = os.path.join(self._manifest_folder, "install.yaml")
        args = ["apply", "-n", self._argo_namespace, "-f", install_yaml]

        await self._kube.ctl(*args)

        if not wait:
            td_install.task_finished(
                msg="installed argocd manifests, installing in background..."
            )
        else:
            td_wait = FreckOpsTaskDesc(
                name="waiting for argocd install", parent=td_install
            )
            td_wait.task_started()
            args = [
                "wait",
                "--for=condition=ready",
                "-n",
                self._argo_namespace,
                "pod",
                "-l",
                "app.kubernetes.io/name=argocd-server",
                "--timeout=300s",
            ]

            while True:
                ctl = await self._kube.ctl(*args, raise_exception=False)
                if await ctl.returncode == 0:
                    break
                else:
                    await asyncio.sleep(2)

            td_wait.task_finished("argocd ready")

            if admin_password:
                td_pw = FreckOpsTaskDesc(
                    name="changing argocd admin password", parent=td_install
                )
                td_pw.task_started()
                await self.change_argo_password(
                    username="admin", new_password=admin_password
                )
                td_pw.task_finished()

            td_install.task_finished()

    def argo_client_session(
        self, username: str = None, password: str = None
    ) -> ArgoCDSession:

        if username is None:
            username = self._argo_user
        if password is None:
            password = self._argo_password

        if password is None:
            raise FrklException(
                msg="Can't create argo client session.", reason="No password provided."
            )

        argo_ctx = ArgoCDSession(
            self._kube,
            argo_user=username,
            argo_password=password,
            namespace=self._argo_namespace,
        )
        return argo_ctx

    async def change_argo_password(
        self,
        username: str = "admin",
        new_password: Optional[str] = None,
        old_password: Optional[str] = None,
    ):

        if old_password is None:
            old_password = self._argo_password

        if old_password is None:

            ret = await self._kube.core_api.list_namespaced_pod(
                namespace=self._argo_namespace
            )

            pw = None
            for i in ret.items:
                if "argocd-server" in i.metadata.name:
                    pw = i.metadata.name
                    break

            if not pw:
                raise Exception("No argocd-server pod, can't find default password.")

            old_password = pw

        if new_password is None:
            new_password = secrets.token_hex(24)

        click.echo(f"  - changing argo password for argo user '{username}'")

        try:
            async with self.argo_client_session(
                username="admin", password=old_password
            ) as argo_session:

                data = {"currentPassword": old_password, "newPassword": new_password}
                response: ClientResponse = await argo_session.session.put(
                    argo_session.api_url("/account/password"),
                    data=json.dumps(data).encode(),
                    ssl=False,
                )

                if response.status >= 200 and response.status < 300:
                    click.echo(f"  - password changed for argo user '{username}'")
                else:
                    resp = await response.text()
                    click.echo(f"  - argo password unchanged: {resp}")

                if username is self._argo_user:
                    self._argo_password = new_password

        except (Exception) as e:
            click.echo(f"  - could not change password: {e}")

    async def install_app(
        self,
        repo_url,
        path=None,
        target_revision="HEAD",
        namespace="default",
        wait=True,
    ):

        repl_dict = {
            "repo_url": repo_url,
            "path": path,
            "target_revision": target_revision,
            "namespace": namespace,
        }
        print(repl_dict)

    #     resource_yaml = Path(
    #         os.path.join(RETAILIATE_K8S_RESOURCES_FOLDER, "05-retailiate.yaml")
    #     )
    #     resource_dict = yaml.load(resource_yaml)
    #
    #     resource = replace_strings_in_obj(
    #         resource_dict, repl_dict, jinja_env=get_global_jinja_env("frkl")
    #     )
    #
    #     click.echo("Installing retailiate")
    #     await self._kube.apply(resource)
    #
    #     async with self.argo_client_session() as argo_session:
    #
    #         name = "ikh"
    #
    #         data = {"dryRun": False, "prune": True, "revision": "HEAD"}
    #
    #         async with argo_session.post(
    #             f"applications/{name}/sync", data=json.dumps(data)
    #         ) as resp:
    #             if not resp.status == 200:
    #                 click.echo(
    #                     f"Error starting 'retailiate' installation: {await resp.text()} (status code: {resp.status}"
    #                 )
    #                 return False
    #
    #     if wait:
    #         click.echo("  - waiting for retailiate install to finish")
    #         await self.wait_for_app("retailiate-api", timeout=400)
    #
    #     return True
    #
    # async def wait_for_app(self, app, timeout=400):
    #
    #     while True:
    #         try:
    #             await self._kube.ctl(
    #                 "wait",
    #                 "-n",
    #                 "ikh",
    #                 "--for=condition=ready",
    #                 "pod",
    #                 "-l",
    #                 f"app={app}",
    #                 f"--timeout={timeout}s",
    #             )
    #             break
    #         except (KubeCtlException):
    #             await asyncio.sleep(2)
    #
    #     click.echo("  - retailiate ready")
