# -*- coding: utf-8 -*-
import getpass
import grp
from typing import Optional

from bring.bring import Bring
from bring.pkg import PkgTing
from freckops.defaults import FRECKOPS_BIN_FOLDER, FRECKOPS_PATH
from frtls.exceptions import FrklException
from frtls.processes import command_exists


def check_dev_env():

    check = {}

    check["docker"] = command_exists("docker", extra_path=FRECKOPS_PATH)

    username = getpass.getuser()
    docker_group = False
    for gr in grp.getgrall():
        gr_name = gr.gr_name
        if gr_name != "docker":
            continue
        mem = gr.gr_mem
        docker_group = username in mem
        break

    check["docker_group"] = docker_group

    check["k3d"] = command_exists("k3d", extra_path=FRECKOPS_PATH)
    check["kubectl"] = command_exists("kubectl", extra_path=FRECKOPS_PATH)
    check["helm"] = command_exists("helm", extra_path=FRECKOPS_PATH)
    check["argo"] = command_exists("argo", extra_path=FRECKOPS_PATH)

    return check


async def provision_dev_env(bring: Bring) -> str:

    context_name = "scratch"

    context = bring.get_context(context_name)
    if context is None:
        raise FrklException(
            msg="Can't provision development environment.",
            reason=f"Can't find context '{context_name}'.",
        )

    pkg_name = "helper_apps"
    pkg: Optional[PkgTing] = await context.get_pkg(pkg_name)

    if pkg is None:
        raise FrklException(
            msg="Can't provision development environment.",
            reason=f"Can't find package '{pkg_name}'.",
        )

    path = await pkg.create_version_folder(
        target={"target": FRECKOPS_BIN_FOLDER, "merge_strategy": "overwrite"}
    )
    return path
