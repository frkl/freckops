# -*- coding: utf-8 -*-
import os
import sys

from appdirs import AppDirs


freckops_app_dirs = AppDirs("freckops", "frkl")

if not hasattr(sys, "frozen"):
    FRECKOPS_MODULE_BASE_FOLDER = os.path.dirname(__file__)
    """Marker to indicate the base folder for the `freckops` module."""
else:
    FRECKOPS_MODULE_BASE_FOLDER = os.path.join(sys._MEIPASS, "freckops")  # type: ignore
    """Marker to indicate the base folder for the `freckops` module."""

FRECKOPS_RESOURCES_FOLDER = os.path.join(FRECKOPS_MODULE_BASE_FOLDER, "resources")

FRECKOPS_BRING_CONTEXT_FOLDER = os.path.join(
    FRECKOPS_RESOURCES_FOLDER, "freckops_bring_context"
)

FRECKOPS_KUBERNETES_MANIFESTS = os.path.join(
    freckops_app_dirs.user_data_dir, "manifests"
)

# PATH-related values
FRECKOPS_BIN_FOLDER = os.path.join(freckops_app_dirs.user_data_dir, "bin")
FRECKOPS_PATH = [FRECKOPS_BIN_FOLDER] + os.environ.get("PATH", os.defpath).split(
    os.pathsep
)


FRECKOPS_BASE_TOPIC = "freckops.tasks"
