# -*- coding: utf-8 -*-
from abc import ABCMeta, abstractmethod
from typing import Iterable


class FreckOpsPlugin(metaclass=ABCMeta):
    @abstractmethod
    def supports(self) -> Iterable[str]:
        pass


class BringFreckOpsPlugin(FreckOpsPlugin):
    def supports(self) -> Iterable[str]:

        return ["bring-pkg"]
