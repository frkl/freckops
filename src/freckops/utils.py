# -*- coding: utf-8 -*-
from freckops.defaults import FRECKOPS_BASE_TOPIC, FRECKOPS_BIN_FOLDER
from frtls.subprocesses import AsyncSubprocess
from frtls.tasks import TaskDesc


class FreckOpsSubprocess(AsyncSubprocess):
    def __init__(
        self,
        command: str,
        *args,
        working_dir: str = None,
        expected_ret_code=0,
        **env_vars,
    ):

        extra_path = FRECKOPS_BIN_FOLDER

        super().__init__(
            command,
            *args,
            working_dir=working_dir,
            expected_ret_code=expected_ret_code,
            extra_path=extra_path,
            **env_vars,
        )


class FreckOpsTaskDesc(TaskDesc):
    def __init__(self, **kwargs):

        kwargs["topic"] = FRECKOPS_BASE_TOPIC
        super().__init__(**kwargs)
