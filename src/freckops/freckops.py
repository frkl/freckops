# -*- coding: utf-8 -*-

"""Main module."""
from typing import Any, Mapping, Optional, Union

from blessings import Terminal
from bring.bring import Bring
from bring.context import BringDynamicContextTing
from bring.pkg import PkgTing
from freckops.defaults import FRECKOPS_BRING_CONTEXT_FOLDER
from frtls.exceptions import FrklException
from frtls.tasks import TaskDesc
from frtls.tasks.task_watcher import TaskWatchManager


class FreckOps(object):
    def __init__(self):

        self._bring: Bring = Bring("bring")
        self._terminal: Terminal = Terminal()

        self._freckops_context: Optional[BringDynamicContextTing] = None

        self._watch_mgmt: TaskWatchManager = TaskWatchManager(
            typistry=self._bring.typistry
        )

        self._initialized = False

    @property
    def terminal(self) -> Terminal:
        return self._terminal

    @property
    def freckops_context(self) -> BringDynamicContextTing:

        if self._freckops_context is not None:
            return self._freckops_context

        self._freckops_context = self._bring.create_ting(  # type: ignore
            "bring_dynamic_context_ting", "bring.context.freckops"
        )
        self._freckops_context.input.set_values(  # type: ignore
            ting_dict={"indexes": [FRECKOPS_BRING_CONTEXT_FOLDER]}
        )
        return self._freckops_context  # type: ignore

    @property
    def bring(self) -> Bring:
        return self._bring

    async def _init(self):

        if self._initialized:
            return

        await self._bring.init()
        await self.freckops_context.get_values("config")
        self._bring.add_context("freckops", self.freckops_context)

    def add_task_watcher(self, watcher: Union[str, Mapping[str, Any]]):

        self._watch_mgmt.add_watcher(watcher=watcher)

    async def install_pkg(
        self,
        target: Union[str, Mapping[str, Any]],
        pkg_name: str,
        pkg_context: Optional[str] = None,
        vars: Mapping[str, Any] = None,
        parent_task_desc: TaskDesc = None,
    ) -> str:

        await self._init()

        if pkg_context is None:
            pkg_context = "freckops"

        context = self.bring.get_context(pkg_context)
        if context is None:
            raise FrklException(
                msg=f"Can't install package '{pkg_name}'.",
                reason=f"No context '{pkg_context}' available.",
            )

        pkg: Optional[PkgTing] = await context.get_pkg(pkg_name)
        if pkg is None:
            raise FrklException(
                msg=f"Can't install package '{pkg_name}'.",
                reason=f"No package '{pkg_name}' in '{pkg_context}'.",
            )

        details = await pkg.create_version_folder(
            vars=vars, target=target, parent_task_desc=parent_task_desc
        )

        return details
