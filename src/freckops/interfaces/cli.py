# -*- coding: utf-8 -*-
import asyncclick as click
import uvloop
from bring.defaults import BRING_TASKS_BASE_TOPIC
from freckops.defaults import FRECKOPS_BASE_TOPIC
from freckops.freckops import FreckOps
from freckops.interfaces.dev import dev
from frtls.cli.exceptions import handle_exc_async
from frtls.cli.logging import logzero_option_async
from frtls.cli.self_command_group import self_command


click.anyio_backend = "asyncio"
uvloop.install()


@click.group()
@click.option(
    "--task-output",
    multiple=True,
    required=False,
    type=str,
    help=f"output plugin(s) for running tasks. available: {', '.join(['terminal', 'simple'])}",
)
@logzero_option_async()
@click.pass_context
@handle_exc_async
async def cli(ctx, task_output):

    ctx.obj = {}
    freckops: FreckOps = FreckOps()

    ctx.obj["freckops"] = freckops

    if not task_output:
        task_output = ["default"]
    for to in task_output:
        freckops.add_task_watcher(
            {
                "type": to,
                "base_topics": [BRING_TASKS_BASE_TOPIC, FRECKOPS_BASE_TOPIC],
                "terminal": freckops.terminal,
            }
        )

    # await freckops.init()


# cli.add_command(kube)
cli.add_command(dev)
cli.add_command(self_command)

if __name__ == "__main__":
    cli(_anyio_backend="asyncio")
