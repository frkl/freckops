# -*- coding: utf-8 -*-
import asyncclick as click
from freckops.defaults import FRECKOPS_BIN_FOLDER
from freckops.dev.dev_env import check_dev_env
from freckops.dev.k3d import K3d
from freckops.freckops import FreckOps
from freckops.k8s.argocd import ArgoCD
from freckops.k8s.kube import Kube
from freckops.utils import FreckOpsTaskDesc
from frtls.cli.exceptions import handle_exc_async
from frtls.formats.output_formats import serialize
from kubernetes_asyncio.config.kube_config import KUBE_CONFIG_DEFAULT_LOCATION


@click.group()
@click.pass_context
async def dev(ctx):
    """freckops development utilities"""

    k3d = K3d()
    ctx.obj["k3d"] = k3d


@dev.group()
@click.pass_context
async def env(ctx):
    """Development enviornment management.

    Commands to help manage and provision a freckops development environment.
    """

    pass


@env.command()
@click.pass_context
async def status(ctx):
    """Display information about development environment."""

    status = check_dev_env()
    click.echo(serialize(status, format="yaml"))


@env.command()
@click.pass_context
async def provision(ctx):
    """Provision/install development requirements.

    This will install helper executables into ~/.local/share/freckops/bin.
    """

    freckops: FreckOps = ctx.obj["freckops"]
    target = {"target": FRECKOPS_BIN_FOLDER, "merge_strategy": "overwrite"}
    await freckops.install_pkg(target=target, pkg_name="helper_apps")


@dev.command()
@click.pass_context
@handle_exc_async
async def init(ctx):
    """Create/initialize a new development project.

    This will create a freckops-compatible project structure, install required helper utilities, and create and
    provision a cluster to be used for development.
    """

    freckops: FreckOps = ctx.obj["freckops"]
    k3d: K3d = ctx.obj["k3d"]
    cluster_name = k3d.default_cluster_name

    td = FreckOpsTaskDesc(
        name="initialize freckops project", msg="installing helper tools"
    )
    td.task_started()
    target = {"target": FRECKOPS_BIN_FOLDER, "merge_strategy": "overwrite"}
    await freckops.install_pkg(
        target=target, pkg_name="helper_apps", parent_task_desc=td
    )

    cluster_names = await k3d.get_cluster_names()
    if cluster_name not in cluster_names:
        await k3d.create_k3d_cluster(parent_task_desc=td)

    td_cfg = FreckOpsTaskDesc(name="retrieving cluster config", parent=td)
    td_cfg.task_started()
    kube: Kube = await k3d.create_kube_obj(cluster_name)
    argo_cd = ArgoCD(freckops=freckops, kube=kube)
    td_cfg.task_finished()
    await argo_cd.install(wait=True, parent_task_desc=td)

    td.task_finished()


@dev.group()
@click.pass_context
async def cluster(ctx):
    """Development-cluster related commands.

    This is using k3d (https://github.com/rancher/k3d) to quickly setup and provision Kubernetes clusters for development.
    """

    pass


@cluster.command()
@click.pass_context
async def merge_config(ctx):

    k3d = ctx.obj["k3d"]

    target = KUBE_CONFIG_DEFAULT_LOCATION

    kube = await k3d.create_kube_obj()
    await kube.merge_kubeconfig(target, set_current_context=True)


@cluster.command()
@click.pass_context
async def unmerge_config(ctx):

    k3d = ctx.obj["k3d"]

    target = KUBE_CONFIG_DEFAULT_LOCATION
    kube = await k3d.create_kube_obj()

    await kube.unmerge_kubeconfig(target)


@cluster.command("list")
@click.pass_context
@handle_exc_async
async def dev_cluster(ctx):
    """List available cluster names."""

    k3d = ctx.obj["k3d"]

    names = await k3d.get_cluster_names()
    click.echo()

    if not names:
        click.echo("No clusters created.")

    else:
        for n in names:
            click.echo(f"  - {n}")


@cluster.command("create")
@click.option(
    "--admin-password",
    prompt=False,
    hide_input=True,
    confirmation_prompt=False,
    help="password for the admin account (defaults to 'password123')",
    default="password123",
)
@click.option(
    "--cloudflare-token",
    prompt=False,
    confirmation_prompt=False,
    required=False,
    hide_input=True,
    default=None,
)
@click.option(
    "--port-forwards",
    "-f",
    multiple=True,
    required=False,
    help="port forwards on container",
)
@click.option(
    "--api-port",
    "-p",
    type=int,
    default=6443,
    help="the cluster api port (auto-selects next available if already used)",
    required=False,
)
@click.option(
    "--merge-config",
    "-m",
    default=False,
    is_flag=True,
    help="merge config of new cluster into default kubeconfig",
)
@click.argument("cluster_name", nargs=1, required=False)
@click.pass_context
@handle_exc_async
async def create_dev_cluster(
    ctx,
    admin_password,
    cloudflare_token,
    port_forwards,
    cluster_name,
    api_port,
    merge_config,
):
    """Spin up a new kubernetes cluster for development."""

    k3d = ctx.obj["k3d"]

    await k3d.create_k3d_cluster(
        cluster_name=cluster_name, port_forwards=port_forwards, api_port=api_port
    )

    if merge_config:
        target = KUBE_CONFIG_DEFAULT_LOCATION

        kube = await k3d.create_kube_obj()
        await kube.merge_kubeconfig(target, set_current_context=True)


@cluster.command("delete")
@click.option(
    "--unmerge",
    "-u",
    help="unmerge config of cluster from default kubeconfig",
    is_flag=True,
    default=False,
)
@click.argument("cluster_name", nargs=1, required=False)
@click.pass_context
@handle_exc_async
async def delete_dev_cluster(ctx, cluster_name, unmerge):
    """Delete an existing development-cluster."""

    k3d = ctx.obj["k3d"]
    if unmerge:
        target = KUBE_CONFIG_DEFAULT_LOCATION
        kube = await k3d.create_kube_obj()
        await kube.unmerge_kubeconfig(target)

    await k3d.delete_k3d_cluster(cluster_name)


@cluster.command("info")
@click.argument("cluster_name", nargs=1, required=False)
@click.pass_context
@handle_exc_async
async def info(ctx, cluster_name):
    """Display information about existing development-cluster."""

    k3d = ctx.obj["k3d"]
    info = await k3d.get_cluster_info(cluster_name)

    print(serialize(info, format="yaml"))
